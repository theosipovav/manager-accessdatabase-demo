﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App20210109
{
    public partial class FormTeachers : Form
    {
        private OleDbConnection connection { get; set; }

        private OleDbDataAdapter dataAdapter { get; set; }

        private DataSet ds;
        private DataSet dsDepartments;

        public string connectionString { get; set; }    // Строка подключения к файлу базы данных Access
        public FormTeachers(string connectionString)
        {
            this.connectionString = connectionString;
            InitializeComponent();
        }

        private void updateForm()
        {
            try
            {
                this.ds = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT teachers.id AS ID, teachers.name AS ФИО, departments.designation AS Кафедра FROM departments INNER JOIN teachers ON departments.id = teachers.departments_id;", this.connection);
                    dataAdapter.Fill(ds);
                    connection.Close();
                }
                DataView dv = ds.Tables[0].DefaultView;
                dataGridViewT.DataSource = dv.ToTable();
                dataGridViewT.Refresh();

                // Выпадающий список "Идентификатор" 
                comboBoxId.Items.Clear();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    comboBoxId.Items.Add(row[0]);
                }
                comboBoxId.Text = "";
                if (comboBoxId.Items.Count > 0)
                {
                    comboBoxId.Text = comboBoxId.Items[0].ToString();
                }


                // Выпадающий список "Кафедры"
                this.dsDepartments = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT id, name, designation FROM departments ORDER BY id;", this.connection);
                    dataAdapter.Fill(dsDepartments);
                    this.connection.Close();
                }
                comboBoxDepartments.Items.Clear();
                foreach (DataRow row in this.dsDepartments.Tables[0].Rows)
                {
                    comboBoxDepartments.Items.Add(row[2]);
                }
                comboBoxDepartments.Text = "";
                if (comboBoxDepartments.Items.Count > 0)
                {
                    comboBoxDepartments.Text = comboBoxDepartments.Items[0].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormTeachers_Load(object sender, EventArgs e)
        {
            updateForm();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "")
            {
                MessageBox.Show("Заполните поля \"ФИО\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string name = textBoxName.Text;


            if (comboBoxDepartments.Text == "")
            {
                MessageBox.Show("Заполните поля \"Кафедра\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string department = comboBoxDepartments.Text;
            try
            {

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row[1].ToString() == name)
                    {
                        MessageBox.Show("Запись с такими параметрами уже существует", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                int departmentId = -1;
                foreach (DataRow row in dsDepartments.Tables[0].Rows)
                {
                    if (row[2].ToString() == department)
                    {
                        if (Int32.TryParse(row[0].ToString(), out departmentId))
                        {
                            break;
                        }
                        else
                        {
                            departmentId = -1;
                        }
                    }
                }
                if (departmentId == -1)
                {
                    MessageBox.Show("Не удалось получить идентификатор кафедры", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("INSERT INTO teachers(name,departments_id) VALUES ('{0}','{1}')", name, departmentId);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            updateForm();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxId.Text == "")
                {
                    MessageBox.Show("Заполните поля \"Идентификатор\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string id = comboBoxId.Text;
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("DELETE * FROM teachers WHERE id={0}", id);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            updateForm();
        }
    }
}
