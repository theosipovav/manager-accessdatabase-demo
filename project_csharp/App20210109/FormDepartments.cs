﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App20210109
{
    public partial class FormDepartments : Form
    {
        private OleDbConnection connection { get; set; }

        private OleDbDataAdapter dataAdapter { get; set; }

        private DataSet ds;
        public string connectionString { get; set; }    // Строка подключения к файлу базы данных Access
        public FormDepartments(string connectionString)
        {
            this.connectionString = connectionString;
            InitializeComponent();
        }
        private void updateForm()
        {
            try
            {
                this.ds = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT departments.id AS ID, departments.name AS Наименование, departments.designation AS Обозначение FROM departments;", this.connection);
                    dataAdapter.Fill(ds);
                    connection.Close();
                }
                DataView dv = ds.Tables[0].DefaultView;
                dataGridViewT.DataSource = dv.ToTable();
                dataGridViewT.Refresh();

                // Выпадающий список "Идентификатор" 
                comboBoxId.Items.Clear();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    comboBoxId.Items.Add(row[0]);
                }
                comboBoxId.Text = "";
                if (comboBoxId.Items.Count > 0)
                {
                    comboBoxId.Text = comboBoxId.Items[0].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


        }
        private void FormDepartments_Load(object sender, EventArgs e)
        {
            updateForm();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxId.Text == "")
                {
                    MessageBox.Show("Заполните поля \"Идентификатор\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string id = comboBoxId.Text;
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("DELETE * FROM departments WHERE id={0}", id);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            updateForm();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxName.Text == "" || textBoxDesignation.Text == "")
                {
                    MessageBox.Show("Заполните поля \"Наименование\" и \"Обозначение\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string name = textBoxName.Text;
                string designation = textBoxDesignation.Text;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row[1].ToString() == name || row[2].ToString() == designation)
                    {
                        MessageBox.Show("Запись с такими параметрами уже существует", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("INSERT INTO departments(name,designation) VALUES ('{0}','{1}')", name, designation);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            updateForm();
        }
    }
}
