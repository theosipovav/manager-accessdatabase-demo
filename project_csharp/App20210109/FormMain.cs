﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App20210109
{
    public partial class FormMain : Form
    {
        FormConnect formConnection;
        private OleDbConnection connection { get; set; }
        private OleDbDataAdapter dataAdapter { get; set; }
        public string connectionString { get; set; }    // Строка подключения к файлу базы данных Access

        private DataSet ds;
        private DataSet dsDisciplines;
        private DataSet dsTeachers;
        private DataSet dsTypes;

        private DataSet dsDepartments;

        public FormMain()
        {
            formConnection = new FormConnect();
            if (formConnection.ShowDialog() == DialogResult.OK)
            {
                this.connectionString = formConnection.connectionString;
                InitializeComponent();
            }
            else
            {
                Environment.Exit(0);
            }
        }
        /// <summary>
        /// Добавить запись
        /// </summary>
        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (comboBoxTeachers.Text == "")
            {
                MessageBox.Show("Заполните поля \"Преподаватель\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string teacher = comboBoxTeachers.Text;



            if (comboBoxDisciplines.Text == "")
            {
                MessageBox.Show("Заполните поля \"Дисциплина\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string discipline = comboBoxDisciplines.Text;

            if (comboBoxTypes.Text == "")
            {
                MessageBox.Show("Заполните поля \"Вид занятия\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string type = comboBoxTypes.Text;






            try
            {
                int teacherId = -1;
                foreach (DataRow row in dsTeachers.Tables[0].Rows)
                {
                    if (row[1].ToString() == teacher)
                    {
                        if (Int32.TryParse(row[0].ToString(), out teacherId))
                        {
                            break;
                        }
                        else
                        {
                            teacherId = -1;
                        }
                    }
                }
                if (teacherId == -1)
                {
                    MessageBox.Show("Не удалось получить идентификатор преподавателя", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }



                int disciplineId = -1;
                foreach (DataRow row in dsDisciplines.Tables[0].Rows)
                {
                    if (row[1].ToString() == discipline)
                    {
                        if (Int32.TryParse(row[0].ToString(), out disciplineId))
                        {
                            break;
                        }
                        else
                        {
                            disciplineId = -1;
                        }
                    }
                }
                if (disciplineId == -1)
                {
                    MessageBox.Show("Не удалось получить идентификатор дисциплины", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int typeId = -1;
                foreach (DataRow row in dsTypes.Tables[0].Rows)
                {
                    if (row[1].ToString() == type)
                    {
                        if (Int32.TryParse(row[0].ToString(), out typeId))
                        {
                            break;
                        }
                        else
                        {
                            typeId = -1;
                        }
                    }
                }
                if (typeId == -1)
                {
                    MessageBox.Show("Не удалось получить идентификатор виды занятия", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int time1;
                if (!Int32.TryParse(textBoxTime1.Text, out time1))
                {
                    MessageBox.Show("Поле \"Часов по видам занятий\" должно содержать целое число", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int time2;
                if (!Int32.TryParse(textBoxTime2.Text, out time2))
                {
                    MessageBox.Show("Поле \"Часов по дисциплине\" должно содержать целое число", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }



                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("INSERT INTO instructions ( teacher_id, discipline_id, type_id, time_type, time_discipline ) VALUES ('{0}','{1}', '{2}','{3}', '{4}')", teacherId, disciplineId, typeId, time1, time2);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            updateForm();
        }


        private void FormMain_Load(object sender, EventArgs e)
        {
            updateForm();
        }


        /// <summary>
        /// Обновить представление
        /// </summary>
        private void updateForm()
        {
            try
            {
                // Представление
                this.ds = new DataSet();
                using (connection = new OleDbConnection(this.connectionString))
                {
                    connection.Open();
                    string sqlString = "SELECT instructions.id AS ID, teachers.name AS Преподаватель, disciplines.name AS Дисциплина, types.name AS [Вид занятия], instructions.time_type AS [Часов по видам занятий], instructions.time_discipline AS [Часов по дисциплине] FROM types INNER JOIN(teachers INNER JOIN (disciplines INNER JOIN instructions ON disciplines.id = instructions.discipline_id) ON teachers.id = instructions.teacher_id) ON types.id = instructions.type_id; ";


                    dataAdapter = new OleDbDataAdapter(sqlString, this.connection);
                    dataAdapter.Fill(ds);
                    connection.Close();
                }
                DataView dv = ds.Tables[0].DefaultView;
                dataGridViewI.DataSource = dv.ToTable();

                // Выпадающий список "Идентификатор" 
                comboBoxId.Items.Clear();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    comboBoxId.Items.Add(row[0]);
                }
                comboBoxId.Text = "";
                if (comboBoxId.Items.Count > 0)
                {
                    comboBoxId.Text = comboBoxId.Items[0].ToString();
                }

                // Выпадающий список "Преподаватель"
                this.dsTeachers = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT id, name FROM teachers ORDER BY id;", this.connection);
                    dataAdapter.Fill(dsTeachers);
                    this.connection.Close();
                }
                comboBoxTeachers.Items.Clear();
                comboBoxTeachers2.Items.Clear();
                foreach (DataRow row in this.dsTeachers.Tables[0].Rows)
                {
                    comboBoxTeachers.Items.Add(row[1]);
                    comboBoxTeachers2.Items.Add(row[1]);
                }
                comboBoxTeachers.Text = "";
                comboBoxTeachers2.Text = "";
                if (comboBoxTeachers.Items.Count > 0)
                {
                    comboBoxTeachers.Text = comboBoxTeachers.Items[0].ToString();
                    comboBoxTeachers2.Text = comboBoxTeachers.Items[0].ToString();
                }

                // Выпадающий список "Дисциплины"
                this.dsDisciplines = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT id, name FROM disciplines ORDER BY id;", this.connection);
                    dataAdapter.Fill(dsDisciplines);
                    this.connection.Close();
                }
                comboBoxDisciplines.Items.Clear();
                comboBoxDisciplines2.Items.Clear();
                foreach (DataRow row in this.dsDisciplines.Tables[0].Rows)
                {
                    comboBoxDisciplines.Items.Add(row[1]);
                    comboBoxDisciplines2.Items.Add(row[1]);
                }
                comboBoxDisciplines.Text = "";
                if (comboBoxDisciplines.Items.Count > 0)
                {
                    comboBoxDisciplines.Text = comboBoxDisciplines.Items[0].ToString();
                    comboBoxDisciplines2.Text = comboBoxDisciplines.Items[0].ToString();
                }

                // Выпадающий список "Виды занятий"
                this.dsTypes = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT id, name FROM types ORDER BY id;", this.connection);
                    dataAdapter.Fill(dsTypes);
                    this.connection.Close();
                }
                comboBoxTypes.Items.Clear();
                comboBoxTypes2.Items.Clear();
                foreach (DataRow row in this.dsTypes.Tables[0].Rows)
                {
                    comboBoxTypes.Items.Add(row[1]);
                    comboBoxTypes2.Items.Add(row[1]);
                }
                comboBoxTypes.Text = "";
                if (comboBoxTypes.Items.Count > 0)
                {
                    comboBoxTypes.Text = comboBoxTypes.Items[0].ToString();
                    comboBoxTypes2.Text = comboBoxTypes.Items[0].ToString();
                }

                // Выпадающий список "Кафедры"
                this.dsDepartments = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT id, name, designation FROM departments ORDER BY id;", this.connection);
                    dataAdapter.Fill(dsDepartments);
                    this.connection.Close();
                }
                comboBoxDepartments2.Items.Clear();
                foreach (DataRow row in this.dsDepartments.Tables[0].Rows)
                {
                    comboBoxDepartments2.Items.Add(row[2]);
                }
                comboBoxDepartments2.Text = "";
                if (comboBoxDepartments2.Items.Count > 0)
                {
                    comboBoxDepartments2.Text = comboBoxDepartments2.Items[0].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);

        }

        private void toolStripButtonTeachers_Click(object sender, EventArgs e)
        {
            FormTeachers formTeachers = new FormTeachers(this.connectionString);
            formTeachers.ShowDialog();
        }

        private void toolStripButtonDepartments_Click(object sender, EventArgs e)
        {
            FormDepartments formDepartments = new FormDepartments(this.connectionString);
            formDepartments.ShowDialog();
        }

        private void toolStripButtonDisciplines_Click(object sender, EventArgs e)
        {
            FormDisciplines formDisciplines = new FormDisciplines(this.connectionString);
            formDisciplines.ShowDialog();
        }

        private void toolStripButtonTypes_Click(object sender, EventArgs e)
        {
            FormTypes formTypes = new FormTypes(this.connectionString);
            formTypes.ShowDialog();
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            (dataGridViewI.DataSource as DataTable).DefaultView.RowFilter = String.Format("Преподаватель like '{0}%' AND Дисциплина like '{1}%'", textBoxFilterT.Text, textBoxFilterD.Text);

        }

        private void comboBoxDepartments2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string queryString = "SELECT instructions.time_type, instructions.time_discipline, departments.designation ";
            queryString += "FROM departments, instructions, teachers ";
            queryString += String.Format("WHERE departments.designation = '{0}' AND teachers.id = instructions.teacher_id AND departments.id = teachers.departments_id;", comboBoxDepartments2.Text);
            using (OleDbConnection connection = new OleDbConnection(this.connectionString))
            {
                OleDbCommand command = new OleDbCommand(queryString, connection);

                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();

                int sum = 0;

                while (reader.Read())
                {
                    int s1;
                    int s2;

                    if (Int32.TryParse(reader[0].ToString(), out s1) && Int32.TryParse(reader[1].ToString(), out s2))
                    {
                        sum += s1 + s2;
                    }

                }
                textBoxMathDepartments.Text = sum.ToString();
                reader.Close();
            }
        }

        private void comboBoxTeachers2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string queryString = "SELECT instructions.time_type, instructions.time_discipline ";
            queryString += "FROM instructions, teachers ";
            queryString += String.Format("WHERE teachers.name = '{0}' AND teachers.id = instructions.teacher_id;", comboBoxTeachers2.Text);
            using (OleDbConnection connection = new OleDbConnection(this.connectionString))
            {
                OleDbCommand command = new OleDbCommand(queryString, connection);

                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();

                int sum = 0;

                while (reader.Read())
                {
                    int s1;
                    int s2;

                    if (Int32.TryParse(reader[0].ToString(), out s1) && Int32.TryParse(reader[1].ToString(), out s2))
                    {
                        sum += s1 + s2;
                    }

                }
                textBoxMathTeachers.Text = sum.ToString();
                reader.Close();
            }
        }

        private void comboBoxDisciplines2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string queryString = "SELECT instructions.time_type, instructions.time_discipline ";
            queryString += "FROM instructions, disciplines ";
            queryString += String.Format("WHERE disciplines.name = '{0}' AND disciplines.id = instructions.discipline_id;", comboBoxDisciplines2.Text);
            using (OleDbConnection connection = new OleDbConnection(this.connectionString))
            {
                OleDbCommand command = new OleDbCommand(queryString, connection);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                int sum = 0;
                while (reader.Read())
                {
                    int s1;
                    int s2;

                    if (Int32.TryParse(reader[0].ToString(), out s1) && Int32.TryParse(reader[1].ToString(), out s2))
                    {
                        sum += s1 + s2;
                    }

                }
                textBoxMathDisciplines.Text = sum.ToString();
                reader.Close();
            }
        }
        private void comboBoxTypes2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string queryString = "SELECT instructions.time_type, instructions.time_discipline ";
            queryString += "FROM instructions, types ";
            queryString += String.Format("WHERE types.name = '{0}' AND types.id = instructions.type_id;", comboBoxTypes2.Text);
            using (OleDbConnection connection = new OleDbConnection(this.connectionString))
            {
                OleDbCommand command = new OleDbCommand(queryString, connection);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                int sum = 0;
                while (reader.Read())
                {
                    int s1;
                    int s2;

                    if (Int32.TryParse(reader[0].ToString(), out s1) && Int32.TryParse(reader[1].ToString(), out s2))
                    {
                        sum += s1 + s2;
                    }

                }
                textBoxMathTypes.Text = sum.ToString();
                reader.Close();
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {

            try
            {
                if (comboBoxId.Text == "")
                {
                    MessageBox.Show("Заполните поля \"Идентификатор\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string id = comboBoxId.Text;
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("DELETE * FROM instructions WHERE id={0}", id);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            updateForm();
        }

        private void buttonFilterRefresh_Click(object sender, EventArgs e)
        {
            updateForm();
        }
    }
}

    
