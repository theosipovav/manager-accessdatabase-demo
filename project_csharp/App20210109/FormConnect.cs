﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App20210109
{
    public partial class FormConnect : Form
    {
        public string connectionString { get; set; }    // Строка подключения к файлу базы данных Access
        public FormConnect()
        {
            if (false)
            {
                DataTable table = new OleDbEnumerator().GetElements();
                string inf = "";
                foreach (DataRow row in table.Rows) inf += row["SOURCES_NAME"] + "\n";
                MessageBox.Show(inf);
            }
            InitializeComponent();
        }
        /// <summary>
        /// Выбрать файл базы данных
        /// </summary>
        private void textBoxPathDatabase_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "База данных Access (*.accdb)|*.accdb|Все файлы (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBoxPathDatabase.Text = dialog.FileName;
            }
            
        }
        /// <summary>
        /// Выйти из программы
        /// </summary>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Выполнить подключение к базе данных
        /// </summary>
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (textBoxConnectionString.Text == "")
            {
                MessageBox.Show("Поле \"Провайдер подключения\" не должно быть пустым", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!File.Exists(textBoxPathDatabase.Text))
            {
                MessageBox.Show("Выберите файл базы данных Access", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            connectionString = String.Format("Provider=Microsoft.ACE.OLEDB.16.0;Data Source={0}", textBoxPathDatabase.Text);
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            this.Close();
            this.DialogResult = DialogResult.OK;
        }
    }
}
