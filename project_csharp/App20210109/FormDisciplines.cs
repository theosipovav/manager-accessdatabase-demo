﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App20210109
{
    public partial class FormDisciplines : Form
    {
        private OleDbConnection connection { get; set; }

        private OleDbDataAdapter dataAdapter { get; set; }

        private DataSet ds;
        public string connectionString { get; set; }    // Строка подключения к файлу базы данных Access
        public FormDisciplines(string connectionString)
        {
            this.connectionString = connectionString;
            InitializeComponent();
        }

        private void updateForm()
        {
            try
            {
                this.ds = new DataSet();
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    dataAdapter = new OleDbDataAdapter("SELECT disciplines.id AS ID, disciplines.name AS Наименование FROM disciplines ORDER BY disciplines.id;", this.connection);
                    dataAdapter.Fill(ds);
                    connection.Close();
                }
                DataView dv = ds.Tables[0].DefaultView;
                dataGridViewT.DataSource = dv.ToTable();
                dataGridViewT.Refresh();

                // Выпадающий список "Идентификатор" 
                comboBoxId.Items.Clear();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    comboBoxId.Items.Add(row[0]);
                }
                comboBoxId.Text = "";
                if (comboBoxId.Items.Count > 0)
                {
                    comboBoxId.Text = comboBoxId.Items[0].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxName.Text == "")
                {
                    MessageBox.Show("Заполните поля \"Наименование\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string name = textBoxName.Text;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row[1].ToString() == name)
                    {
                        MessageBox.Show("Запись с такими параметрами уже существует", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("INSERT INTO disciplines(name) VALUES ('{0}')", name);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            updateForm();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxId.Text == "")
                {
                    MessageBox.Show("Заполните поля \"Идентификатор\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string id = comboBoxId.Text;
                using (this.connection = new OleDbConnection(this.connectionString))
                {
                    this.connection.Open();
                    string sqlString = String.Format("DELETE * FROM disciplines WHERE id={0}", id);
                    OleDbCommand command = new OleDbCommand(sqlString, connection);
                    int result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            updateForm();
        }

        private void FormDisciplines_Load(object sender, EventArgs e)
        {
            updateForm();
        }
    }
}
