﻿namespace App20210109
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.buttonFilterRefresh = new System.Windows.Forms.Button();
            this.buttonFilter = new System.Windows.Forms.Button();
            this.textBoxFilterT = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFilterD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxId = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxTime2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxTime1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxDisciplines = new System.Windows.Forms.ComboBox();
            this.comboBoxTypes = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxTeachers = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridViewI = new System.Windows.Forms.DataGridView();
            this.toolStripButtonDepartments = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTeachers = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExit = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonDisciplines = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTypes = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxDepartments2 = new System.Windows.Forms.ComboBox();
            this.comboBoxTeachers2 = new System.Windows.Forms.ComboBox();
            this.comboBoxDisciplines2 = new System.Windows.Forms.ComboBox();
            this.comboBoxTypes2 = new System.Windows.Forms.ComboBox();
            this.textBoxMathDepartments = new System.Windows.Forms.TextBox();
            this.textBoxMathTeachers = new System.Windows.Forms.TextBox();
            this.textBoxMathDisciplines = new System.Windows.Forms.TextBox();
            this.textBoxMathTypes = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewI)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonFilterRefresh
            // 
            this.buttonFilterRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFilterRefresh.Location = new System.Drawing.Point(459, 16);
            this.buttonFilterRefresh.Name = "buttonFilterRefresh";
            this.buttonFilterRefresh.Size = new System.Drawing.Size(92, 31);
            this.buttonFilterRefresh.TabIndex = 8;
            this.buttonFilterRefresh.Text = "Сбросить";
            this.buttonFilterRefresh.UseVisualStyleBackColor = true;
            this.buttonFilterRefresh.Click += new System.EventHandler(this.buttonFilterRefresh_Click);
            // 
            // buttonFilter
            // 
            this.buttonFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFilter.Location = new System.Drawing.Point(459, 53);
            this.buttonFilter.Name = "buttonFilter";
            this.buttonFilter.Size = new System.Drawing.Size(92, 31);
            this.buttonFilter.TabIndex = 7;
            this.buttonFilter.Text = "Применить";
            this.buttonFilter.UseVisualStyleBackColor = true;
            this.buttonFilter.Click += new System.EventHandler(this.buttonFilter_Click);
            // 
            // textBoxFilterT
            // 
            this.textBoxFilterT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilterT.Location = new System.Drawing.Point(97, 38);
            this.textBoxFilterT.Name = "textBoxFilterT";
            this.textBoxFilterT.Size = new System.Drawing.Size(356, 20);
            this.textBoxFilterT.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxFilterD);
            this.panel1.Controls.Add(this.buttonFilterRefresh);
            this.panel1.Controls.Add(this.buttonFilter);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxFilterT);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(12, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(554, 111);
            this.panel1.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Дисциплина";
            // 
            // textBoxFilterD
            // 
            this.textBoxFilterD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilterD.Location = new System.Drawing.Point(97, 64);
            this.textBoxFilterD.Name = "textBoxFilterD";
            this.textBoxFilterD.Size = new System.Drawing.Size(356, 20);
            this.textBoxFilterD.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Преподаватель";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Фильтр";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxId);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.buttonRemove);
            this.groupBox2.Location = new System.Drawing.Point(3, 299);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(194, 111);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Удалить";
            // 
            // comboBoxId
            // 
            this.comboBoxId.FormattingEnabled = true;
            this.comboBoxId.Location = new System.Drawing.Point(6, 33);
            this.comboBoxId.Name = "comboBoxId";
            this.comboBoxId.Size = new System.Drawing.Size(181, 21);
            this.comboBoxId.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Идентификатор:";
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemove.Location = new System.Drawing.Point(90, 82);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(97, 23);
            this.buttonRemove.TabIndex = 1;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Преподаватель:";
            // 
            // buttonCreate
            // 
            this.buttonCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCreate.Location = new System.Drawing.Point(91, 261);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(97, 23);
            this.buttonCreate.TabIndex = 1;
            this.buttonCreate.Text = "Добавить";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxTime2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBoxTime1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.comboBoxDisciplines);
            this.groupBox1.Controls.Add(this.comboBoxTypes);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBoxTeachers);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonCreate);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 290);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Добавить";
            // 
            // textBoxTime2
            // 
            this.textBoxTime2.Location = new System.Drawing.Point(6, 207);
            this.textBoxTime2.Name = "textBoxTime2";
            this.textBoxTime2.Size = new System.Drawing.Size(181, 20);
            this.textBoxTime2.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 191);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Часов по дисциплине:";
            // 
            // textBoxTime1
            // 
            this.textBoxTime1.Location = new System.Drawing.Point(6, 167);
            this.textBoxTime1.Name = "textBoxTime1";
            this.textBoxTime1.Size = new System.Drawing.Size(181, 20);
            this.textBoxTime1.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Часов по видам занятий:";
            // 
            // comboBoxDisciplines
            // 
            this.comboBoxDisciplines.FormattingEnabled = true;
            this.comboBoxDisciplines.Location = new System.Drawing.Point(6, 74);
            this.comboBoxDisciplines.Name = "comboBoxDisciplines";
            this.comboBoxDisciplines.Size = new System.Drawing.Size(182, 21);
            this.comboBoxDisciplines.TabIndex = 9;
            // 
            // comboBoxTypes
            // 
            this.comboBoxTypes.FormattingEnabled = true;
            this.comboBoxTypes.Location = new System.Drawing.Point(6, 114);
            this.comboBoxTypes.Name = "comboBoxTypes";
            this.comboBoxTypes.Size = new System.Drawing.Size(182, 21);
            this.comboBoxTypes.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Вид занятия:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Дисциплина:";
            // 
            // comboBoxTeachers
            // 
            this.comboBoxTeachers.FormattingEnabled = true;
            this.comboBoxTeachers.Location = new System.Drawing.Point(6, 33);
            this.comboBoxTeachers.Name = "comboBoxTeachers";
            this.comboBoxTeachers.Size = new System.Drawing.Size(181, 21);
            this.comboBoxTeachers.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Location = new System.Drawing.Point(572, 34);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 573);
            this.panel3.TabIndex = 8;
            // 
            // dataGridViewI
            // 
            this.dataGridViewI.AllowUserToAddRows = false;
            this.dataGridViewI.AllowUserToDeleteRows = false;
            this.dataGridViewI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewI.Location = new System.Drawing.Point(12, 154);
            this.dataGridViewI.Name = "dataGridViewI";
            this.dataGridViewI.ReadOnly = true;
            this.dataGridViewI.Size = new System.Drawing.Size(554, 321);
            this.dataGridViewI.TabIndex = 7;
            // 
            // toolStripButtonDepartments
            // 
            this.toolStripButtonDepartments.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDepartments.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDepartments.Image")));
            this.toolStripButtonDepartments.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDepartments.Name = "toolStripButtonDepartments";
            this.toolStripButtonDepartments.Size = new System.Drawing.Size(61, 22);
            this.toolStripButtonDepartments.Text = "Кафедры";
            this.toolStripButtonDepartments.Click += new System.EventHandler(this.toolStripButtonDepartments_Click);
            // 
            // toolStripButtonTeachers
            // 
            this.toolStripButtonTeachers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonTeachers.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTeachers.Image")));
            this.toolStripButtonTeachers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTeachers.Name = "toolStripButtonTeachers";
            this.toolStripButtonTeachers.Size = new System.Drawing.Size(96, 22);
            this.toolStripButtonTeachers.Text = "Преподаватели";
            this.toolStripButtonTeachers.Click += new System.EventHandler(this.toolStripButtonTeachers_Click);
            // 
            // toolStripButtonExit
            // 
            this.toolStripButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonExit.Image")));
            this.toolStripButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExit.Name = "toolStripButtonExit";
            this.toolStripButtonExit.Size = new System.Drawing.Size(46, 22);
            this.toolStripButtonExit.Text = "Выход";
            this.toolStripButtonExit.Click += new System.EventHandler(this.toolStripButtonExit_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonExit,
            this.toolStripButtonTeachers,
            this.toolStripButtonDepartments,
            this.toolStripButtonDisciplines,
            this.toolStripButtonTypes});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonDisciplines
            // 
            this.toolStripButtonDisciplines.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDisciplines.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDisciplines.Image")));
            this.toolStripButtonDisciplines.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDisciplines.Name = "toolStripButtonDisciplines";
            this.toolStripButtonDisciplines.Size = new System.Drawing.Size(83, 22);
            this.toolStripButtonDisciplines.Text = "Дисциплины";
            this.toolStripButtonDisciplines.Click += new System.EventHandler(this.toolStripButtonDisciplines_Click);
            // 
            // toolStripButtonTypes
            // 
            this.toolStripButtonTypes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonTypes.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTypes.Image")));
            this.toolStripButtonTypes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTypes.Name = "toolStripButtonTypes";
            this.toolStripButtonTypes.Size = new System.Drawing.Size(86, 22);
            this.toolStripButtonTypes.Text = "Виды занятий";
            this.toolStripButtonTypes.Click += new System.EventHandler(this.toolStripButtonTypes_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.textBoxMathTypes);
            this.panel2.Controls.Add(this.textBoxMathDisciplines);
            this.panel2.Controls.Add(this.textBoxMathTeachers);
            this.panel2.Controls.Add(this.textBoxMathDepartments);
            this.panel2.Controls.Add(this.comboBoxTypes2);
            this.panel2.Controls.Add(this.comboBoxDisciplines2);
            this.panel2.Controls.Add(this.comboBoxTeachers2);
            this.panel2.Controls.Add(this.comboBoxDepartments2);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(12, 481);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(554, 126);
            this.panel2.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Объемы учебной работы ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(206, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "по кафедрам";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(173, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "по преподавателям";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(187, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "по дисциплинам ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(182, 96);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "по видам занятий";
            // 
            // comboBoxDepartments2
            // 
            this.comboBoxDepartments2.FormattingEnabled = true;
            this.comboBoxDepartments2.Location = new System.Drawing.Point(286, 10);
            this.comboBoxDepartments2.Name = "comboBoxDepartments2";
            this.comboBoxDepartments2.Size = new System.Drawing.Size(181, 21);
            this.comboBoxDepartments2.TabIndex = 14;
            this.comboBoxDepartments2.SelectedIndexChanged += new System.EventHandler(this.comboBoxDepartments2_SelectedIndexChanged);
            // 
            // comboBoxTeachers2
            // 
            this.comboBoxTeachers2.FormattingEnabled = true;
            this.comboBoxTeachers2.Location = new System.Drawing.Point(286, 37);
            this.comboBoxTeachers2.Name = "comboBoxTeachers2";
            this.comboBoxTeachers2.Size = new System.Drawing.Size(181, 21);
            this.comboBoxTeachers2.TabIndex = 15;
            this.comboBoxTeachers2.SelectedIndexChanged += new System.EventHandler(this.comboBoxTeachers2_SelectedIndexChanged);
            // 
            // comboBoxDisciplines2
            // 
            this.comboBoxDisciplines2.FormattingEnabled = true;
            this.comboBoxDisciplines2.Location = new System.Drawing.Point(286, 65);
            this.comboBoxDisciplines2.Name = "comboBoxDisciplines2";
            this.comboBoxDisciplines2.Size = new System.Drawing.Size(182, 21);
            this.comboBoxDisciplines2.TabIndex = 16;
            this.comboBoxDisciplines2.SelectedIndexChanged += new System.EventHandler(this.comboBoxDisciplines2_SelectedIndexChanged);
            // 
            // comboBoxTypes2
            // 
            this.comboBoxTypes2.FormattingEnabled = true;
            this.comboBoxTypes2.Location = new System.Drawing.Point(285, 92);
            this.comboBoxTypes2.Name = "comboBoxTypes2";
            this.comboBoxTypes2.Size = new System.Drawing.Size(182, 21);
            this.comboBoxTypes2.TabIndex = 17;
            this.comboBoxTypes2.SelectedIndexChanged += new System.EventHandler(this.comboBoxTypes2_SelectedIndexChanged);
            // 
            // textBoxMathDepartments
            // 
            this.textBoxMathDepartments.Location = new System.Drawing.Point(483, 10);
            this.textBoxMathDepartments.Name = "textBoxMathDepartments";
            this.textBoxMathDepartments.Size = new System.Drawing.Size(59, 20);
            this.textBoxMathDepartments.TabIndex = 18;
            // 
            // textBoxMathTeachers
            // 
            this.textBoxMathTeachers.Location = new System.Drawing.Point(483, 37);
            this.textBoxMathTeachers.Name = "textBoxMathTeachers";
            this.textBoxMathTeachers.Size = new System.Drawing.Size(59, 20);
            this.textBoxMathTeachers.TabIndex = 19;
            // 
            // textBoxMathDisciplines
            // 
            this.textBoxMathDisciplines.Location = new System.Drawing.Point(483, 65);
            this.textBoxMathDisciplines.Name = "textBoxMathDisciplines";
            this.textBoxMathDisciplines.Size = new System.Drawing.Size(59, 20);
            this.textBoxMathDisciplines.TabIndex = 20;
            // 
            // textBoxMathTypes
            // 
            this.textBoxMathTypes.Location = new System.Drawing.Point(483, 92);
            this.textBoxMathTypes.Name = "textBoxMathTypes";
            this.textBoxMathTypes.Size = new System.Drawing.Size(59, 20);
            this.textBoxMathTypes.TabIndex = 21;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 613);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.dataGridViewI);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormMain";
            this.Text = "Поручения преподавателям";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewI)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFilterRefresh;
        private System.Windows.Forms.Button buttonFilter;
        private System.Windows.Forms.TextBox textBoxFilterT;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridViewI;
        private System.Windows.Forms.ToolStripButton toolStripButtonDepartments;
        private System.Windows.Forms.ToolStripButton toolStripButtonTeachers;
        private System.Windows.Forms.ToolStripButton toolStripButtonExit;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ComboBox comboBoxDisciplines;
        private System.Windows.Forms.ComboBox comboBoxTypes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxTeachers;
        private System.Windows.Forms.ToolStripButton toolStripButtonDisciplines;
        private System.Windows.Forms.ToolStripButton toolStripButtonTypes;
        private System.Windows.Forms.TextBox textBoxTime2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxTime1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFilterD;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxMathTypes;
        private System.Windows.Forms.TextBox textBoxMathDisciplines;
        private System.Windows.Forms.TextBox textBoxMathTeachers;
        private System.Windows.Forms.TextBox textBoxMathDepartments;
        private System.Windows.Forms.ComboBox comboBoxTypes2;
        private System.Windows.Forms.ComboBox comboBoxDisciplines2;
        private System.Windows.Forms.ComboBox comboBoxTeachers2;
        private System.Windows.Forms.ComboBox comboBoxDepartments2;
    }
}